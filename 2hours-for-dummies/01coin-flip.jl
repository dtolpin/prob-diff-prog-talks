### A Pluto.jl notebook ###
# v0.14.0

using Markdown
using InteractiveUtils

# ╔═╡ 944c536f-333b-40cc-9214-497642efea8b
begin
	# Using Base modules.
	using Random

	# Load a plotting library.
	using Plots, StatsPlots

	# Load the distributions library.
	using Distributions
end

# ╔═╡ cca139c7-3270-467c-bb87-b2da586a84d9
begin
	# Load Turing and MCMCChains.
	using Turing, MCMCChains
end

# ╔═╡ 1c42b75a-9625-11eb-26c8-afbe32d8fa06
md"# Coin flip

_(adapted from [Turing Tutorials](https://github.com/TuringLang/TuringTutorials))_

We are given a coin. We flip the coin N times and want to know whether the coin is _fair_ or _biased_, that is whether the probabilities of heads and tails are the same or different. 

This is a basic problem almost every probabilistic programming tutorial starts with. This is also a popular illustration for Bayesian reasoning about probabilities: according to the Bayes rule, 

$$\Pr(\theta|y) = \frac {\Pr(\theta)\Pr(y|\theta)} {\Pr(y)} \propto \Pr(\theta)\Pr(y|\theta)$$

That is, if we have observations $y$ and parameter of interest $\theta$, we can reason about $\theta$ if we have some prior knowledge $\Pr(\theta)$ about $\theta$ and conditional probability of $y$ given $\theta$ , $\Pr(y|\theta)$.
"

# ╔═╡ 11c0a4b3-94d6-43cf-89d4-ba12bad2cbbc
md"## Coin Flipping Without Turing
The following example illustrates the effect of updating our beliefs with every piece of new evidence we observe. In particular, assume that we are unsure about the probability of heads in a coin flip. To get an intuitive understanding of what \"updating our beliefs\" is, we will visualize the probability of heads in a coin flip after each observed evidence.

First, let's load some of the packages we need to flip a coin (`Random`, `Distributions`) and show our results (`Plots`). You will note that Turing is not an import here — we do not need it for this example. If you are already familiar with posterior updates, you can proceed to the next step.
"

# ╔═╡ 7b47ca03-4e0a-4c5c-afb1-b1b1dcc96a34
md"Next, we configure our posterior update model. First, let's set the true probability that any coin flip will turn up heads and set the number of coin flips we will show our model:
"

# ╔═╡ 321d039e-c402-4664-87eb-4a74cb3e57dc
begin
	# Set the true probability of heads in a coin.
	p_true = 0.5

	# Iterate from having seen 0 observations to 100 observations.
	Ns = 0:100
end;

# ╔═╡ 90e9ef9b-8104-4e4e-b219-7c91952d2101
md"We will now use the Bernoulli distribution to flip 100 coins, and collect the results in a variable called `data`:
"

# ╔═╡ bbb502b2-d14d-4ada-9ead-9a7fc6647b4a
begin
	# Draw data from a Bernoulli distribution, i.e. draw heads or tails.
	# Random.seed!(12)
	data = rand(Bernoulli(p_true), last(Ns))

	# Here's what the first five coin flips look like:
	data[1:5]
end

# ╔═╡ 6f2bb862-73dd-404a-85d8-ab1190e1df2c
md"After flipping all our coins, we want to set a prior belief about what we think the distribution of coin flips look like. In this case, we are going to choose a common prior distribution called the [Beta](https://en.wikipedia.org/wiki/Beta_distribution) distribution.
"

# ╔═╡ d1000fdd-bcbc-40cd-8924-fc5489e2ee4f
# Our prior belief about the probability of heads in a coin toss.
prior_belief = Beta(1, 1);

# ╔═╡ bbd931ca-e25f-48fb-8b7c-51a9cee1a3be
md"With our priors set and our data at hand, we can perform Bayesian inference.

This is a fairly simple process. We expose one additional coin flip to our model every iteration, such that the first run only sees the first coin flip, while the last iteration sees all the coin flips. Then, we set the `updated_belief` variable to an updated version of the original Beta distribution that accounts for the new proportion of heads and tails. 
"

# ╔═╡ 61f3ab74-3da7-4aa3-9bc1-75404e32d9d5
begin
	# Make an animation.
	animation = @gif for (i, N) in enumerate(Ns)

		# Count the number of heads and tails.
		heads = sum(data[1:i-1])
		tails = N - heads

		# Update our prior belief in closed form 
		# (this is possible because we use a conjugate prior).
		updated_belief = Beta(prior_belief.α + heads, prior_belief.β + tails)

		# Plotting
		plot(updated_belief, 
			size = (500, 250), 
			title = "Updated belief after $N observations",
			xlabel = "probability of heads", 
			ylabel = "", 
			legend = nothing,
			xlim = (0,1),
			fill=0, α=0.3, w=3)
		vline!([p_true])
	end;
	animation
end

# ╔═╡ 322f0d12-2030-4d21-a3c1-971e8c69d558
md"The animation above shows that with increasing evidence our belief about the probability of heads in a coin flip slowly adjusts towards the true value. The orange line in the animation represents the true probability of seeing heads on a single coin flip, while the mode of the distribution shows what the model believes the probability of a heads is given the evidence it has seen.
"

# ╔═╡ 80e557c7-7974-45eb-b003-e289d342776c
md"## Coin Flipping With Turing

In the previous example, we used the fact that our prior distribution is a [conjugate prior](https://en.wikipedia.org/wiki/Conjugate_prior). Note that a closed-form expression (the `updated_belief` expression) for the posterior is not accessible in general and usually does not exist for more interesting models. 

We are now going to move away from the closed-form expression above and specify the same model using **Turing**. To do so, we will first need to import `Turing`, `MCMCChains`, `Distributions`, and `StatPlots`. `MCMCChains` is a library built by the Turing team to help summarize Markov Chain Monte Carlo (MCMC) simulations, as well as a variety of utility functions for diagnostics and visualizations.
"

# ╔═╡ 663482b8-4e3a-4430-9fd8-17df7d05f036
md"First, we define the coin-flip model using Turing."

# ╔═╡ f8801dd6-e78b-43e6-9f70-c29262bc4f03
@model coinflip(y) = begin
    
    # Our prior belief about the probability of heads in a coin.
    p ~ Beta(1, 1)
    
    # The number of observations.
    N = length(y)
    for n in 1:N
        # Heads or tails of a coin are drawn from a Bernoulli distribution.
        y[n] ~ Bernoulli(p)
    end
end;

# ╔═╡ 7e59ed42-c8ca-40bd-8da1-894ee5f5ed89
md"After defining the model, we can approximate the posterior distribution by drawing samples from the distribution. In this example, we use a [Hamiltonian Monte Carlo](https://en.wikipedia.org/wiki/Hamiltonian_Monte_Carlo) sampler to draw these samples. Later tutorials will give more information on the samplers available in Turing and discuss their use for different models."

# ╔═╡ 524e939a-1470-440b-beb6-cf0396ff8123
begin
	# Settings of the Hamiltonian Monte Carlo (HMC) sampler.
	iterations = 5000
	ϵ = 0.05
	τ = 10

	# Start sampling.
	chain = sample(coinflip(data), HMC(ϵ, τ), iterations, progress=false)
end;

# ╔═╡ 1d3f7b21-77ec-4f6b-9372-2c9d46756d33
md"After finishing the sampling process, we can visualize the posterior distribution approximated using Turing against the posterior distribution in closed-form. We can extract the chain data from the sampler using the `Chains(chain[:p])` function, exported from the `MCMCChain` module. `Chains(chain[:p])` creates an instance of the `Chain` type which summarizes the MCMC simulation — the `MCMCChain` module supports numerous tools for plotting, summarizing, and describing variables of type `Chain`.
"

# ╔═╡ 71344d10-66a3-4c18-bd70-4baaa22ab195
begin
	# Construct summary of the sampling process for the parameter p, 
	# i.e. the probability of heads in a coin.
	p_summary = chain[:p]
	plot(p_summary, label="posterior of p", seriestype = :histogram)
end

# ╔═╡ 78c3c9e5-d29d-4eab-9e1d-b68a1b75ad2d
md"Now we can build our plot:"

# ╔═╡ 79557f48-eefa-47b4-98f2-d192b2d39f10
let
	# Compute the posterior distribution in closed-form.
	N = length(data)
	heads = sum(data)
	updated_belief = Beta(prior_belief.α + heads, prior_belief.β + N - heads)

	# Visualize a blue density plot of the approximate 
	# posterior distribution using HMC (see Chain 1 in the legend).
	p = plot(p_summary, seriestype = :density, 
		     xlim = (0,1), label="MC approximation",
		     legend = :best, w = 2, c = :blue)

	# Visualize a green density plot of posterior distribution 
	# in closed-form.
	plot!(p, range(0, stop = 1, length = 100), 
		  pdf.(Ref(updated_belief), range(0, stop = 1, length = 100)), 
		  xlabel = "probability of heads", ylabel = "", title = "",
		  xlim = (0,1), label = "Closed-form",
		  fill=0, α=0.3, w=3, c = :lightgreen)

	# Visualize the true probability of heads in red.
	vline!(p, [p_true], label = "True probability", c = :red)
end

# ╔═╡ 644ea639-ef25-4b44-a634-0f478c973bac
md"As we can see, the Turing model closely approximates the true probability. Hopefully this tutorial has provided an easy-to-follow, yet informative introduction to Turing's simpler applications. More advanced usage will be demonstrated in later tutorials."

# ╔═╡ Cell order:
# ╟─1c42b75a-9625-11eb-26c8-afbe32d8fa06
# ╟─11c0a4b3-94d6-43cf-89d4-ba12bad2cbbc
# ╠═944c536f-333b-40cc-9214-497642efea8b
# ╟─7b47ca03-4e0a-4c5c-afb1-b1b1dcc96a34
# ╠═321d039e-c402-4664-87eb-4a74cb3e57dc
# ╟─90e9ef9b-8104-4e4e-b219-7c91952d2101
# ╠═bbb502b2-d14d-4ada-9ead-9a7fc6647b4a
# ╟─6f2bb862-73dd-404a-85d8-ab1190e1df2c
# ╠═d1000fdd-bcbc-40cd-8924-fc5489e2ee4f
# ╟─bbd931ca-e25f-48fb-8b7c-51a9cee1a3be
# ╠═61f3ab74-3da7-4aa3-9bc1-75404e32d9d5
# ╟─322f0d12-2030-4d21-a3c1-971e8c69d558
# ╟─80e557c7-7974-45eb-b003-e289d342776c
# ╠═cca139c7-3270-467c-bb87-b2da586a84d9
# ╟─663482b8-4e3a-4430-9fd8-17df7d05f036
# ╠═f8801dd6-e78b-43e6-9f70-c29262bc4f03
# ╟─7e59ed42-c8ca-40bd-8da1-894ee5f5ed89
# ╠═524e939a-1470-440b-beb6-cf0396ff8123
# ╟─1d3f7b21-77ec-4f6b-9372-2c9d46756d33
# ╠═71344d10-66a3-4c18-bd70-4baaa22ab195
# ╟─78c3c9e5-d29d-4eab-9e1d-b68a1b75ad2d
# ╠═79557f48-eefa-47b4-98f2-d192b2d39f10
# ╟─644ea639-ef25-4b44-a634-0f478c973bac
