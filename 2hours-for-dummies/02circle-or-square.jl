### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 5efd96d2-9553-11eb-1862-a5041e2664a4
begin
	using Flux, Plots, Images, Distributions, Random
	using Turing
	using PlutoUI: with_terminal
end

# ╔═╡ 4c2bf89a-8c06-4c92-8ce8-3edf28e4f03d
md"# Circle or square?

We want to distinguish between squares and circles given a few noisy points from either. 
"

# ╔═╡ b3b1e907-f672-4ab6-81ac-9763b52c5db8
md"First, we need to get the data. Here is a simulator which draws either a noisy square or a noisy circle, with a given probability.
"

# ╔═╡ 8f263700-9554-11eb-13a9-5f85748cea5f
function simulate(n; pSquare=0.5, σ=0.025)
	r = rand(Gamma(10, 0.1))
	pt = Array{Float64,2}(undef, n, 2)
	α = rand(Uniform(0, 1), n)
	isSquare = rand(Bernoulli(pSquare))
	a = r .+ rand(Normal(0, σ), n)
	if isSquare
		b = r.*(-1 .+ 2 .* α) .+ rand(Normal(0, σ), n)
		for i in 1:n
			side = rand(Categorical(4))
			if side == 1
				pt[i, 1] = a[i]
				pt[i, 2] = b[i]
			elseif side == 2
				pt[i, 1] = -b[i]
				pt[i, 2] = a[i]
			elseif side == 3
				pt[i, 1] = -a[i]
				pt[i, 2] = -b[i]
			elseif side == 4
				pt[i, 1] = b[i]
				pt[i, 2] = -a[i]
			end
		end
	else
		φ = 2*π.*α
		pt[:, 1] = a.*cos.(φ)
		pt[:, 2] = a.*sin.(φ)
	end
	pt, isSquare
end

# ╔═╡ 64a8d690-6735-4d15-9c51-707fdf51014b
md"We can visualize the generated shapes. The more points are generated, the easier it is to recognize the shape. We choose the number of points where the shape is already recognizable by a human eye, but is still vague.
"

# ╔═╡ d0b024d9-d219-43e3-9300-f17aa22e5733
N = 10;

# ╔═╡ 07e161d8-955a-11eb-0e55-b51da968eefa
let
	pt, isSquare = simulate(N)
	Plots.scatter(pt[:, 1], pt[:, 2], 
		color=isSquare ? "red" : "blue", 
		xlims=(-2, 2), 
		ylims=(-2, 2), 
		size=(300, 300),
		label=missing)
end

# ╔═╡ 3e590eef-5265-491c-9913-bb3e4071cc5a
md"## Deep learning

Our first approach to this problem uses neural networks. We define a rather simple three layer fully connected network. This should be enough to fit a couple thousands of different shapes.
"

# ╔═╡ 90b1abb0-955c-11eb-3a72-69fcd0300219
function make_model(n)
	model = Chain(
		Dense(n, 10*n, relu),
		Dense(10*n, 10*n, relu),
		Dense(10*n, 2), 
		softmax)
end

# ╔═╡ 374cff25-7cb7-41df-b0aa-2f6725dcb23d
md"We won't always have the data to train the network, but for this study we assume that we do. We generate enough labeled samples — the training data — to train the model.
"

# ╔═╡ 96d8bbfc-955f-11eb-0e03-1d2c62ac771c
begin
	M = 1000
	data = Array{Float64, 2}(undef, N*2, M)
	labels = Array{Float64}(undef, M)
	for  i = 1:M
		pt, isSquare = simulate(N)
		data[:, i] = reshape(pt, :, 1)	
		labels[i] = isSquare
	end
end

# ╔═╡ 4275a459-f0f9-4cf6-a6ad-b06f5d760869
md"When presented a shape, the network returns whether it is a square or a circle. We compute the prediction accuracy as normalized Jackard distance — fraction of predictions where the network's prediction did not match the label. 

Note that even if the network does not agree with the label, that does not necessary mean a 'mistake': one can sample points from a square such that they can also come from a circle, especially a noisy one.
"

# ╔═╡ 297f1503-8f71-4c64-b2af-5815fd194396
function accuracy(model, x, y) 
	mean(abs.(Flux.onecold(y) - Flux.onecold(model(x))))
end

# ╔═╡ eebdfd33-84b8-46d0-9040-43291416649e
labels

# ╔═╡ 6fb702e8-38c9-4558-a29c-bef9b1e8f4a9
md"### Randomly initialized model

First, let's compute the accuracy before training. It should be close to 0.5 — our predictions are random guesses, so they will be right half of the time.
"

# ╔═╡ 8b2d965e-955e-11eb-2827-f79fa210728a
begin 
	x = data
	y = Flux.onehotbatch(labels, 0:1)
	model = make_model(2*N)
	accuracy(model, x, y)
end

# ╔═╡ 6c21d870-cd66-41e1-b91c-1abc0e114175
md"### Trained model

We train the model to miminize the discrepancy between predictions and lables on the training data. Even with our simple neural architecture, we can achieve a remarkable accuracy.
"

# ╔═╡ 53c77b62-9561-11eb-30c8-972c6bcd5be0
let 
	ps = Flux.params(model)
	opt = ADAM()
	batches = Flux.Data.DataLoader((x, y), batchsize=16)
	Epochs = 100
	for epoch in 1:Epochs
		for (xb, yb) in batches
			gs = gradient(ps) do 
				Flux.Losses.logitcrossentropy(model(xb), yb)
			end
			Flux.Optimise.update!(opt, ps, gs)
		end
		
	end
	accuracy(model, x, y)
end

# ╔═╡ 51d1c5c9-d3f2-4a57-9d6e-595addc32105
let i = rand(1:M)
	pt = reshape(data[:, i], :, 2)
	isSquare = labels[i]
	Plots.scatter(pt[:, 1], pt[:, 2])
	y = model(reshape(pt, :, 1))
	Plots.scatter(pt[:, 1], pt[:, 2], label=missing, 
		          color=isSquare>0 ? "red" : "blue"),
	Plots.histogram([0, 1], weights=y, bins=3, label=missing, xlabel="pSquare")
end
	

# ╔═╡ a43edc12-6954-4701-9b02-84d00f4c673e
md"However this spectacular accuracy on training data does not translate to good performance on new shapes,  simulated by the same procedure. The predictions are only slightly better than random. 
"

# ╔═╡ 08dac026-85d1-4a2a-abe7-0aba33637d65
begin
	tdata = Array{Float64, 2}(undef, N*2, M)
	tlabels = Array{Float64}(undef, M)
	for  i = 1:M
		pt, isSquare = simulate(N)
		tdata[:, i] = reshape(pt, :, 1)	
		tlabels[i] = isSquare
	end
end

# ╔═╡ d58c61ae-d031-478c-afb1-b183aecb6e75
let x = tdata,
	y = Flux.onehotbatch(tlabels, 0:1)
	accuracy(model, x, y)
end

# ╔═╡ 325441ad-11b7-424d-93e6-7e1f4477517c
md"The neural model didn't really learn to distinguish between circles and square. And we have little means to find out what actually it **did** learn to recognize.
"

# ╔═╡ 1de9fb5e-9562-11eb-3f0a-294a4deadaea
let (pt, isSquare) = simulate(N)
	y = model(reshape(pt, :, 1))
	Plots.scatter(pt[:, 1], pt[:, 2], label=missing, color=isSquare ? "red" : "blue"),
	Plots.histogram([0, 1], weights=y, bins=3, xlabel="pSquare", label=missing)
end

# ╔═╡ 7dfea9a5-2880-49ed-abe7-fa264794087a
md"Some call it **overfit**. But there is more than just overfit here. Our model did not encode our general human knowledge about circles and squares. It just selected a likely hypothesis which suits the training data. 
"

# ╔═╡ c2fe7a21-61a7-4d5e-8b3d-da0e45cfa097
md"### Data transformation

One way to improve the performance of a discriminative model is to transform the data such that it is easier to learn for the model **what we want it to learn**. Here, our anticipation is that the model learns to distinguish between shapes, square or circle, ignoring the variability in sizes.

"

# ╔═╡ 9f8488cb-fca7-414c-8e53-efff427ab570
md"#### Scaling

The simplest thing we can do is to scale the data so that the extreme points are always at the edge of the rectangle.
"

# ╔═╡ 2a94d5ae-d311-407c-9be9-ab087e2e48da
function stretch(pt) 
	d = 2*maximum(abs.(pt))
	pt ./ d
end

# ╔═╡ f57b46ae-871f-4fc8-9b56-35c913ae6d62
let 
	pt, isSquare = simulate(N)
	stretch(pt)
	Plots.scatter(pt[:, 1], pt[:, 2], label=missing, 
		          color=isSquare>0 ? "red" : "blue")
end

# ╔═╡ 2575cb33-de48-48c4-9211-7559a8d9d136
begin
	sdata = Array{Float64, 2}(undef, 2*N, M)
	for  i = 1:M
		sdata[:, i] = reshape(stretch(reshape(data[:, i], :, 2)), :, 1)
	end
end

# ╔═╡ edc8d436-2b17-42c2-9f81-8537e282f048
begin 
	sx = sdata
	sy = y
	smodel = make_model(2*N)
	accuracy(smodel, sx, sy)
end	

# ╔═╡ 72c01b9f-d92e-43ce-a2b0-e247c5424b60
let 
	ps = Flux.params(smodel)
	opt = ADAM()
	batches = Flux.Data.DataLoader((sx, sy), batchsize=16)
	Epochs = 100
	for epoch in 1:Epochs
		for (xb, yb) in batches
			gs = gradient(ps) do 
				Flux.Losses.logitcrossentropy(smodel(xb), yb)
			end
			Flux.Optimise.update!(opt, ps, gs)
		end
		
	end
	accuracy(smodel, sx, sy)
end

# ╔═╡ 788ff820-7a11-489a-a9ef-30d610e9dada
begin
	tsdata = Array{Float64, 2}(undef, 2*N, M)
	for  i = 1:M
		tsdata[:, i] = reshape(stretch(reshape(tdata[:, i], :, 2)), :, 1)
	end
end

# ╔═╡ 65d16d88-7f53-4873-bfff-1b45ba2e9558
let x = tsdata,
	y = Flux.onehotbatch(tlabels, 0:1)
	accuracy(smodel, x, y)
end

# ╔═╡ 908f2dd3-857a-40c1-9927-284c7848b5a1
md"#### Pixelization


We can also represent the data so that the shape is easy to recognize. In addition to scaling, we will discretize the data into pixel maps of fixed size.
"

# ╔═╡ 5d3da117-8bba-4ea7-a5b8-b2e5ab688874
begin
	K = 10
	function pixelize(pt; size=K)
		pic= zeros(size, size)
		d = 2*maximum(abs.(pt))
		for i in 1:length(pt[:, 1])
			x = pt[i, 1]/d
			y = pt[i, 2]/d
			j = 1 + Int(round((size-1)*(0.5 + x)))
			k = 1 + Int(round((size-1)*(0.5 + y)))
			pic[j, k] = 1.
		end
		pic
	end
end

# ╔═╡ 79a0af06-89be-4b71-96bc-e4551cb658d6
let 
	pt, iSquare = simulate(N)
	pic = pixelize(pt)
	if iSquare 
		plot(colorview(RGB, ones(K, K), 1 .- pic, 1 .- pic))
	else
		plot(colorview(RGB, 1 .- pic, 1 .- pic, ones(K, K)))

	end
end

# ╔═╡ b12eda51-9a8c-4136-b5ae-7d1f9cc8af27
md"Note that we provided extra information to our ‘machine learning’ pipeline, namely, that the differentiating feature is the shape rather than the size. We cannot easily add this information to the model itself, and if we later want to distinguish between small and big pictures, this encoding cannot be used."

# ╔═╡ 545a76b7-dafe-454e-9573-3bfc1f3fceda
begin
	picdata = Array{Float64, 2}(undef, K*K, M)
	for  i = 1:M
		picdata[:, i] = reshape(pixelize(reshape(data[:, i], :, 2)), :, 1)
	end
end

# ╔═╡ ef781734-d88e-4333-85e2-184f2a28b309
begin 
	picx = picdata
	picy = y
	picmodel = make_model(K*K)
	accuracy(picmodel, picx, picy)
end	

# ╔═╡ 14797b30-798a-4b59-9e84-f408d733287a
let 
	ps = Flux.params(picmodel)
	opt = ADAM()
	batches = Flux.Data.DataLoader((picx, picy), batchsize=16)
	Epochs = 100
	for epoch in 1:Epochs
		for (xb, yb) in batches
			gs = gradient(ps) do 
				Flux.Losses.logitcrossentropy(picmodel(xb), yb)
			end
			Flux.Optimise.update!(opt, ps, gs)
		end
		
	end
	accuracy(picmodel, picx, picy)
end

# ╔═╡ d68db730-e62f-4c87-bceb-4320de6ad9b6
begin
	tpicdata = Array{Float64, 2}(undef, K*K, M)
	for  i = 1:M
		tpicdata[:, i] = reshape(pixelize(reshape(tdata[:, i], :, 2)), :, 1)
	end
end

# ╔═╡ 43714240-638c-4424-b118-461b2df93aa9
let x = tpicdata,
	y = Flux.onehotbatch(tlabels, 0:1)
	accuracy(picmodel, x, y)
end

# ╔═╡ 6ec03277-cde3-46ee-ad2e-0cda0a70ef37
md"## Probabilistic programming

Instead of training a black-box _discriminative_ model, we can write a generative model which reflects our hypothesis that we are looking at either a centered square or a centered circle. 

It is simple to sample noisly from a circle. Square is a little more challenging, but we can approximate a square by four two-dimensional normal distributions:
"

# ╔═╡ ef244bbc-879d-4737-a565-9ff8295713c4
let r=rand(Gamma(2, 0.5)), σ = 0.01, τ=0.5*r, 
	square = rand(MixtureModel([
		MvNormal([r, 0], [σ, τ]),
		MvNormal([0, r], [τ, σ]),
		MvNormal([-r, 0], [σ, τ]),
		MvNormal([0, -r], [τ, σ])]),
		10000)
	Plots.scatter(square[1, :], square[2, :], label=missing)
end

# ╔═╡ ed870328-96fc-4339-bb7e-2aee47c7457c
md"### Generative model

Based on this, we can write a generative model. Note that our generative model draws either shapes resembling squares or shapes resembling circles, but does it in a different way than the simulator, and with a different distribution of sizes. We do not have to mimick the data perfectly --- what we need to specify is a reasonable set of hypotheses. _Probabilistic programming_ helps us accomplish this.
"

# ╔═╡ 809f68c8-9568-11eb-097a-510a04943f79
@model function ros(pt, ρ; pSquare = 0.5, σ=0.02) 
	N = length(pt[:,1])
	r ~ Gamma(4, 0.25)
	isSquare ~ Bernoulli(pSquare)
	if isSquare
		for i in 1:N
			pt[i, :] ~ MixtureModel([
					MvNormal([r, 0], [σ*r, r]),
					MvNormal([0, r], [r, σ*r]),
					MvNormal([-r, 0], [σ*r, r]),
					MvNormal([0, -r], [r, σ*r])])	
		end
	else
		for i in 1:N
			ρ[i] ~ Normal(r, 1.5σ)
		end
	end
end

# ╔═╡ 2fe07d66-e278-4e00-8649-a423b1d404b9
let err = 0
	for i = 1:M
		pt = reshape(tdata[:, i], :, 2)
		chain = sample(ros(pt, sqrt.(pt[:, 1].^2 .+ pt[:, 2].^2)), MH(), 1000)
		isSquare = mean(chain[:isSquare][:, 1]) > 0.5
		if isSquare != tlabels[i]
			err += 1
		end
	end
	err / M
end

# ╔═╡ 3a16b291-5926-4dd5-9502-e3cfd048ab3a
md"Our simple model does not need any training data to distringuish circles and squares with high confidence. However, training data can be used with probabilistic programs when desirable. Such models are caller _hierarchical_.
"

# ╔═╡ bf67b40e-956b-11eb-2f38-23d0a52d25f1
let (pt, isSquare) = simulate(N)
	ρ = sqrt.(pt[:, 1].^2 .+ pt[:, 2].^2)
	chain = sample(ros(pt, sqrt.(pt[:, 1].^2 .+ pt[:, 2].^2)), MH(), 10000)
	Plots.scatter(pt[:, 1], pt[:, 2], color=isSquare ? "red" : "blue"), 
	histogram(chain[:isSquare][:, 1], bins=2, xlims=(0, 1.5), 
		      xlabel="pSquare", label=missing),
	histogram(chain[:r][:, 1], xlabel="r", label=missing)
end

# ╔═╡ Cell order:
# ╟─4c2bf89a-8c06-4c92-8ce8-3edf28e4f03d
# ╠═5efd96d2-9553-11eb-1862-a5041e2664a4
# ╟─b3b1e907-f672-4ab6-81ac-9763b52c5db8
# ╠═8f263700-9554-11eb-13a9-5f85748cea5f
# ╟─64a8d690-6735-4d15-9c51-707fdf51014b
# ╠═d0b024d9-d219-43e3-9300-f17aa22e5733
# ╠═07e161d8-955a-11eb-0e55-b51da968eefa
# ╟─3e590eef-5265-491c-9913-bb3e4071cc5a
# ╠═90b1abb0-955c-11eb-3a72-69fcd0300219
# ╟─374cff25-7cb7-41df-b0aa-2f6725dcb23d
# ╠═96d8bbfc-955f-11eb-0e03-1d2c62ac771c
# ╟─4275a459-f0f9-4cf6-a6ad-b06f5d760869
# ╠═297f1503-8f71-4c64-b2af-5815fd194396
# ╠═eebdfd33-84b8-46d0-9040-43291416649e
# ╟─6fb702e8-38c9-4558-a29c-bef9b1e8f4a9
# ╠═8b2d965e-955e-11eb-2827-f79fa210728a
# ╟─6c21d870-cd66-41e1-b91c-1abc0e114175
# ╠═53c77b62-9561-11eb-30c8-972c6bcd5be0
# ╠═51d1c5c9-d3f2-4a57-9d6e-595addc32105
# ╟─a43edc12-6954-4701-9b02-84d00f4c673e
# ╠═08dac026-85d1-4a2a-abe7-0aba33637d65
# ╠═d58c61ae-d031-478c-afb1-b183aecb6e75
# ╟─325441ad-11b7-424d-93e6-7e1f4477517c
# ╠═1de9fb5e-9562-11eb-3f0a-294a4deadaea
# ╟─7dfea9a5-2880-49ed-abe7-fa264794087a
# ╟─c2fe7a21-61a7-4d5e-8b3d-da0e45cfa097
# ╟─9f8488cb-fca7-414c-8e53-efff427ab570
# ╠═2a94d5ae-d311-407c-9be9-ab087e2e48da
# ╠═f57b46ae-871f-4fc8-9b56-35c913ae6d62
# ╠═2575cb33-de48-48c4-9211-7559a8d9d136
# ╠═edc8d436-2b17-42c2-9f81-8537e282f048
# ╠═72c01b9f-d92e-43ce-a2b0-e247c5424b60
# ╠═788ff820-7a11-489a-a9ef-30d610e9dada
# ╠═65d16d88-7f53-4873-bfff-1b45ba2e9558
# ╟─908f2dd3-857a-40c1-9927-284c7848b5a1
# ╠═5d3da117-8bba-4ea7-a5b8-b2e5ab688874
# ╠═79a0af06-89be-4b71-96bc-e4551cb658d6
# ╟─b12eda51-9a8c-4136-b5ae-7d1f9cc8af27
# ╠═545a76b7-dafe-454e-9573-3bfc1f3fceda
# ╠═ef781734-d88e-4333-85e2-184f2a28b309
# ╠═14797b30-798a-4b59-9e84-f408d733287a
# ╠═d68db730-e62f-4c87-bceb-4320de6ad9b6
# ╠═43714240-638c-4424-b118-461b2df93aa9
# ╟─6ec03277-cde3-46ee-ad2e-0cda0a70ef37
# ╠═ef244bbc-879d-4737-a565-9ff8295713c4
# ╟─ed870328-96fc-4339-bb7e-2aee47c7457c
# ╠═809f68c8-9568-11eb-097a-510a04943f79
# ╠═2fe07d66-e278-4e00-8649-a423b1d404b9
# ╟─3a16b291-5926-4dd5-9502-e3cfd048ab3a
# ╠═bf67b40e-956b-11eb-2f38-23d0a52d25f1
